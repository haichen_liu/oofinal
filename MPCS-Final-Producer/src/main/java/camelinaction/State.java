package camelinaction;

abstract public class State {
	StateMachine tm;
	abstract void buy(Double AverageBidPrice);
	abstract void sale(double askPrice);
}
