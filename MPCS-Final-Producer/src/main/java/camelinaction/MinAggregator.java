package camelinaction;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class MinAggregator implements AggregationStrategy{

	@Override
	public Exchange aggregate(Exchange old_msg, Exchange new_msg) {
		// TODO Auto-generated method stub
		if(old_msg==null){
			return new_msg;
		}
		Double old_price=old_msg.getIn().getBody(Double.class);
		Double new_price=new_msg.getIn().getBody(Double.class);

		old_msg.getIn().setBody(old_price>new_price?new_price:old_price); 
		return old_msg;
	}

}
