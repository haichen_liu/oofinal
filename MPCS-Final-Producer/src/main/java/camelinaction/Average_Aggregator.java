package camelinaction;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class Average_Aggregator implements AggregationStrategy{
//	public int count=0;
	@Override
	public Exchange aggregate(Exchange old_msg, Exchange new_msg) {
//		count = (count+1)%20;
		double count=0;
		count++;
		if(old_msg==null){
			return new_msg;
		}
		
		double new_price=new_msg.getIn().getBody(Double.class);
		double old_average=old_msg.getIn().getBody(Double.class);

		
//		Double new_average=((new_price==null?0:new_price)
//				+(old_average==null?0:old_average)*(count-1))*count;
		
		old_msg.getIn().setBody((new_price+old_average));
		
		return old_msg;
	}

}
