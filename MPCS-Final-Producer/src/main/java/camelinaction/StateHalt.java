package camelinaction;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StateHalt extends State{
	double SalePrice = 39.8;	 
	@Override
	void sale(double AverageBidPrice) {

		if(AverageBidPrice*(1+0.001)>SalePrice){
			tm.state = new Sale();
		}
	}
	
	@Override
	void buy(Double AskPrice){
		
		if(AskPrice*(1-0.001)<SalePrice){
			tm.state = new StateBuy();
		}
	}

}
