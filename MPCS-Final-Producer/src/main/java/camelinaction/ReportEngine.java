package camelinaction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ReportEngine {
	protected ReportEngine(){
		System.out.println("report");
	}
	public static ReportEngine getInstance(){
		if(instance==null){
			synchronized(ReportEngine.class){
				if(instance==null){
					instance=new ReportEngine();
					it=new Iterator();
				}
			}
		}
		return instance;
	} 
	
	private static ReportEngine instance=null;
	public static Iterator it;
	int count=0;
	public void push(Double _d){
		it.push_back(_d);
	}
	
	public void printReport() throws IOException{
		System.out.println("print");
		double average=0;
		double STD_DEV=0;
		double max=0;
		double min=Double.MAX_VALUE;
		int count=0;
		if(it.isEmpty()){
			File file=new File("data/Report/Report.txt");
			FileWriter fw=new FileWriter(file, true);
			PrintWriter writer = new PrintWriter(fw);
			writer.println("Empty");
			writer.close();
			return;
		}
//		
		
		for(double item=it.first();!it.isDone();item=it.next()){
//			
			System.out.printf("%f\n", item);
			double temp=item;
			average+=temp;
			
			max=(max>item)?max:item;
			min=(min<item)?min:item;
			count++;
		}
		average=average/count;
		
		for(double item=it.first();!it.isDone();item=it.next()){
			STD_DEV+=(item-average)*(item-average);
		}

		
		
		STD_DEV=Math.sqrt(STD_DEV/count);
		File file=new File("data/Report/Report.txt");
		FileWriter fw=new FileWriter(file, true);
		PrintWriter writer = new PrintWriter(fw);
		writer.printf("%d \n", it.count);
		writer.printf("The average price is: %f \n", average);
		writer.printf("The STD_DEV is: %f \n", STD_DEV);
		writer.printf("The maximal price is: %f \n", max);
		writer.printf("The minimal price is: %f \n", min);
		writer.close();
	}
}
