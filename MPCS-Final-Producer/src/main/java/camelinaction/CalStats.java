/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camelinaction;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;



public class CalStats implements Runnable{
    public void run() {
        // create CamelContext
    	try{
//    		Thread.sleep(2000);
    		
        CamelContext context = new DefaultCamelContext();

        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = 
        	new ActiveMQConnectionFactory("tcp://localhost:62020");
        context.addComponent("jms",
            JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
        
        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {

        	public void configure() {


            	//Statistic Each Value of Each Stock
        		
            	
            	//Statistic Each Value of Each Stock
                from("jms:topic:IBM_BidPrice")
                	.aggregate(header("BidPrice"), new Average_Aggregator()).completionSize(20)
                	.process(new Processor(){
    					@Override
    					public void process(Exchange exchange) throws Exception {
    						double sum=exchange.getIn().getBody(Double.class);
    						exchange.getIn().setBody(sum/20);
    						exchange.getIn().setHeader("BidPriceAverage", "BidPriceAverage");
    					}
		             })
                	.to("jms:topic:IBM_BID_PRICE_AVE");
                
                
                from("jms:topic:IBM_BidPrice")
            	.aggregate(header("BidPrice"), new MinAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMin", "BidPriceMin");
					}
	             })
            	.to("jms:topic:IBM_BID_PRICE_MIN");
                
                
                from("jms:topic:IBM_BidPrice")
            	.aggregate(header("BidPrice"), new MaxAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMax", "BidPriceMax");
					}
	             })
            	.to("jms:topic:IBM_BID_PRICE_MAX");
        		
        		
                from("jms:topic:ORCL_BidPrice")
                	.aggregate(header("BidPrice"), new Average_Aggregator()).completionSize(20)
                	.process(new Processor(){
    					@Override
    					public void process(Exchange exchange) throws Exception {
    						double sum=exchange.getIn().getBody(Double.class);
    						exchange.getIn().setBody(sum/20);
    						exchange.getIn().setHeader("BidPriceAverage", "BidPriceAverage");
    					}
		             })
                	.to("jms:topic:ORCL_BID_PRICE_AVE");
                
                
                from("jms:topic:ORCL_BidPrice")
            	.aggregate(header("BidPrice"), new MinAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMin", "BidPriceMin");
					}
	             })
            	.to("jms:topic:ORCL_BID_PRICE_MIN");
                
                
                from("jms:topic:ORCL_BidPrice")
            	.aggregate(header("BidPrice"), new MaxAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMax", "BidPriceMax");
					}
	             })
            	.to("jms:topic:ORCL_BID_PRICE_MAX");
        		
        		
                from("jms:topic:MSFT_BidPrice")
                	.aggregate(header("BidPrice"), new Average_Aggregator()).completionSize(20)
                	.process(new Processor(){
    					@Override
    					public void process(Exchange exchange) throws Exception {
    						double sum=exchange.getIn().getBody(Double.class);
    						exchange.getIn().setBody(sum/20);
    						exchange.getIn().setHeader("BidPriceAverage", "BidPriceAverage");
    					}
		             })
                	.to("jms:topic:MSFT_BID_PRICE_AVE");
                //TODO this is a topic
                
                from("jms:topic:MSFT_BidPrice")
            	.aggregate(header("BidPrice"), new MinAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMin", "BidPriceMin");
					}
	             })
            	.to("jms:topic:MSFT_BID_PRICE_MIN");
                
                
                from("jms:topic:MSFT_BidPrice")
            	.aggregate(header("BidPrice"), new MaxAggregator()).completionSize(20)
            	.process(new Processor(){
					@Override
					public void process(Exchange exchange) throws Exception {
//						double sum=exchange.getIn().getBody(Double.class);
//						exchange.getIn().setBody(sum/20);
						exchange.getIn().setHeader("BidPriceMax", "BidPriceMax");
					}
	             })
            	.to("jms:topic:MSFT_BID_PRICE_MAX");
                
            }
        });

        // start the route and let it do its work
        context.start();
        Thread.sleep(20000);

        // stop the CamelContext
        context.stop();
        }catch(Exception e){
        	
        }
    }
}
