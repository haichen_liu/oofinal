/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package camelinaction;

import static org.apache.camel.builder.PredicateBuilder.not;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;

public class Z_StateMachine implements Runnable{
	static public Z_State state;
	static public double Average;
    public void run(){
        // create CamelContext
    	try{
        CamelContext context = new DefaultCamelContext();
        state = new Z_StateHalt();
        // connect to ActiveMQ JMS broker listening on localhost on port 61616
        ConnectionFactory connectionFactory = 
        	new ActiveMQConnectionFactory("tcp://localhost:62020");
        context.addComponent("jms",
            JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
        
        // add our route to the CamelContext
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                //from("file:data/inbox?noop=true").to("file:data/outbox");
            //Get the average price from queue
            	from("jms:topic:MSFT_BID_PRICE_AVE")
            		.process(new Processor(){

						@Override
						public void process(Exchange arg0) throws Exception {
							// TODO Auto-generated method stub
							//Get the new average price 
							state=new Z_Sale();
//							Average=arg0.getIn().getBody(Double.class);
							state.sale(arg0.getIn().getBody(Double.class));
//							state.Handle(arg0.getIn().getBody(Double.class), 0);
						}
            			
            		});
            	//buy or sale based on the average price and sale price 
            	from("jms:topic:MPCS_51050_Final_MSFT_Sale")
            		.choice()
            			.when(not(header("MachineID").isEqualTo("z_tradingengine")))
	            		.process(new Processor(){
	            			@Override
	            			public void process(Exchange arg0) throws Exception{
	            				state=new Z_StateBuy();
	            				String price_str=arg0.getIn().getBody(String.class);
	            				double price=Double.parseDouble(price_str);
	            				state.buy(price);
	            			}
	            		});
//                
            }
        });

        // start the route and let it do its work
        Thread.sleep(3000);
        context.start();
        Thread.sleep(20000);

        // stop the CamelContext
        context.stop();
    	}catch(Exception e){
    		
    	}
    }
}
